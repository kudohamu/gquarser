package gquarser

import (
	"net/url"
	"testing"
)

func TestNewStringParser(t *testing.T) {
	u, _ := url.Parse("?hoge=2")
	_, err := NewStringParser("test").Parse(u.Query())
	if err == nil {
		t.Errorf("expected err is not nil but nil")
	}

	u, _ = url.Parse("?hoge=2")
	actual, err := NewStringParser("test").Default("actual").Parse(u.Query())
	if actual != "actual" {
		t.Errorf("expected actual, but %s", actual)
	}
	if err != nil {
		t.Errorf("expected err is nil but not nil")
	}

	u, _ = url.Parse("?hoge=2&test=foo")
	actual, err = NewStringParser("test").Default("actual").Parse(u.Query())
	if actual != "foo" {
		t.Errorf("expected foo, but %s", actual)
	}
	if err != nil {
		t.Errorf("expected err is nil but not nil")
	}

}

func BenchmarkNewStringParser(b *testing.B) {
	u, _ := url.Parse("?hoge=2&test=bar")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		NewStringParser("test").Default("foo").Parse(u.Query())
	}
}

func TestNewIntParser(t *testing.T) {
	u, _ := url.Parse("?hoge=2")
	_, err := NewIntParser("test").Parse(u.Query())
	if err == nil {
		t.Errorf("expected err is not nil but nil")
	}

	u, _ = url.Parse("?hoge=2")
	actual, err := NewIntParser("test").Default(10).Parse(u.Query())
	if actual != 10 {
		t.Errorf("expected 10, but %d", actual)
	}
	if err != nil {
		t.Errorf("expected err is nil but not nil")
	}

	u, _ = url.Parse("?hoge=2&test=20")
	actual, _ = NewIntParser("test").Default(10).Parse(u.Query())
	if actual != 20 {
		t.Errorf("expected 20, but %d", actual)
	}

	u, _ = url.Parse("?hoge=2&test=-10")
	actual, _ = NewIntParser("test").Default(10).Lower(-5).Parse(u.Query())
	if actual != -5 {
		t.Errorf("expected -5, but %d", actual)
	}

	u, _ = url.Parse("?hoge=2&test=100")
	actual, _ = NewIntParser("test").Default(10).Lower(-5).Upper(50).Parse(u.Query())
	if actual != 50 {
		t.Errorf("expected -50, but %d", actual)
	}
}

func BenchmarkNewIntParser(b *testing.B) {
	u, _ := url.Parse("?hoge=2&test=200")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		NewIntParser("test").Default(10).Lower(-5).Upper(100).Parse(u.Query())
	}
}

func TestNewFloatParser(t *testing.T) {
	u, _ := url.Parse("?hoge=2")
	_, err := NewFloatParser("test", 64).Parse(u.Query())
	if err == nil {
		t.Errorf("expected err is not nil but nil")
	}

	u, _ = url.Parse("?hoge=2")
	actual, err := NewFloatParser("test", 64).Default(1.5).Parse(u.Query())
	if actual != 1.5 {
		t.Errorf("expected 1.5, but %f", actual)
	}
	if err != nil {
		t.Errorf("expected err is nil but not nil")
	}

	u, _ = url.Parse("?hoge=2&test=2.5")
	actual, _ = NewFloatParser("test", 64).Default(1.5).Parse(u.Query())
	if actual != 2.5 {
		t.Errorf("expected 2.5, but %f", actual)
	}

	u, _ = url.Parse("?hoge=2&test=-10.5")
	actual, _ = NewFloatParser("test", 64).Default(10).Lower(-5.5).Parse(u.Query())
	if actual != -5.5 {
		t.Errorf("expected -5.5, but %f", actual)
	}

	u, _ = url.Parse("?hoge=2&test=100.5")
	actual, _ = NewFloatParser("test", 64).Default(10).Lower(-5.5).Upper(50.5).Parse(u.Query())
	if actual != 50.5 {
		t.Errorf("expected -50.5, but %f", actual)
	}
}

func BenchmarkNewFloatParser(b *testing.B) {
	u, _ := url.Parse("?hoge=2&test=20.0")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		NewFloatParser("test", 64).Default(10).Lower(-5.5).Upper(10.5).Parse(u.Query())
	}
}

func TestNewStringsParser(t *testing.T) {
	u, _ := url.Parse("?hoge=2")
	_, err := NewStringsParser("test").Parse(u.Query())
	if err == nil {
		t.Errorf("expected err is not nil but nil")
	}

	u, _ = url.Parse("?hoge=2")
	actual, err := NewStringsParser("test").Default([]string{"a"}).Parse(u.Query())
	expected := []string{"a"}
	if err != nil {
		t.Errorf("expected err is nil but not nil")
	}
	for k := range expected {
		if actual[k] != expected[k] {
			t.Errorf("expected %s, but %s", expected[k], actual[k])
		}
	}

	u, _ = url.Parse("?hoge=2&test=a+b+c")
	actual, _ = NewStringsParser("test").Default([]string{"a"}).Parse(u.Query())
	expected = []string{"a", "b", "c"}
	for k := range expected {
		if actual[k] != expected[k] {
			t.Errorf("expected %s, but %s", expected[k], actual[k])
		}
	}
}

func BenchmarkNewStringsParser(b *testing.B) {
	u, _ := url.Parse("?hoge=2&test=foo,bar,baz")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		NewStringsParser("test").Default([]string{"hoge"}).Parse(u.Query())
	}
}

func TestNewIntsParser(t *testing.T) {
	u, _ := url.Parse("?hoge=2")
	_, err := NewIntsParser("test").Parse(u.Query())
	if err == nil {
		t.Errorf("expected err is not nil but nil")
	}

	u, _ = url.Parse("?hoge=2")
	actual, err := NewIntsParser("test").Default([]int{1}).Parse(u.Query())
	expected := []int{1}
	if err != nil {
		t.Errorf("expected err is nil but not nil")
	}
	for k := range expected {
		if actual[k] != expected[k] {
			t.Errorf("expected %d, but %d", expected[k], actual[k])
		}
	}

	u, _ = url.Parse("?hoge=2&test=1+-10+100")
	actual, _ = NewIntsParser("test").Default([]int{0}).Lower(-5).Upper(50).Parse(u.Query())
	expected = []int{1, -5, 50}
	for k := range expected {
		if actual[k] != expected[k] {
			t.Errorf("expected %d, but %d", expected[k], actual[k])
		}
	}
}

func BenchmarkNewIntsParser(b *testing.B) {
	u, _ := url.Parse("?hoge=2&test=20,30,40")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		NewIntsParser("test").Default([]int{}).Delimiter(",").Lower(-5).Upper(10).Parse(u.Query())
	}
}

func TestNewFloatsParser(t *testing.T) {
	u, _ := url.Parse("?hoge=2")
	_, err := NewFloatsParser("test", 64).Parse(u.Query())
	if err == nil {
		t.Errorf("expected err is not nil but nil")
	}

	u, _ = url.Parse("?hoge=2")
	actual, err := NewFloatsParser("test", 64).Default([]float64{1.5}).Parse(u.Query())
	expected := []float64{1.5}
	if err != nil {
		t.Errorf("expected err is nil but not nil")
	}
	for k := range expected {
		if actual[k] != expected[k] {
			t.Errorf("expected %f, but %f", expected[k], actual[k])
		}
	}

	u, _ = url.Parse("?hoge=2&test=1.5+-10.5+100.5")
	actual, _ = NewFloatsParser("test", 64).Default([]float64{0}).Lower(-5.5).Upper(50.5).Parse(u.Query())
	expected = []float64{1.5, -5.5, 50.5}
	for k := range expected {
		if actual[k] != expected[k] {
			t.Errorf("expected %f, but %f", expected[k], actual[k])
		}
	}
}

func BenchmarkNewFloatsParser(b *testing.B) {
	u, _ := url.Parse("?hoge=2&test=20.5,30.5,40.5")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		NewFloatsParser("test", 64).Default([]float64{}).Delimiter(",").Lower(-5.5).Upper(10.5).Parse(u.Query())
	}
}
