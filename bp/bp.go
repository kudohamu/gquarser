package bp

// BaseParser has common property for query parser.
type BaseParser struct {
	Field    string //field to parse
	HasDef   bool
	HasUpper bool
	HasLower bool
}
