package stringsparser

import (
	"errors"
	"net/url"
	"strings"

	"github.com/kudohamu/gquarser/bp"
)

// StringsParser is string field parser that have delimiter.
type StringsParser struct {
	bp.BaseParser
	def   []string
	Delim string
	upper string
	lower string
}

// Default set default return value when field is empty or nothing.
func (ssp *StringsParser) Default(d []string) *StringsParser {
	ssp.def = d
	ssp.HasDef = true
	return ssp
}

// Delimiter set field delimiter.
func (ssp *StringsParser) Delimiter(d string) *StringsParser {
	ssp.Delim = d
	return ssp
}

// Parse strings field.
func (ssp *StringsParser) Parse(v url.Values) ([]string, error) {
	q := strings.Split(v.Get(ssp.Field), ssp.Delim)
	if len(q) == 1 && q[0] == "" {
		if !ssp.HasDef {
			return []string{}, errors.New("cannot find field value")
		}
		q = ssp.def
	}
	if ssp.HasUpper {
		for i := range q {
			if ssp.upper < q[i] {
				q[i] = ssp.upper
			}
		}
	}
	if ssp.HasLower {
		for i := range q {
			if q[i] < ssp.lower {
				q[i] = ssp.lower
			}
		}
	}
	return q, nil
}
