package intparser

import (
	"errors"
	"net/url"
	"strconv"

	"github.com/kudohamu/gquarser/bp"
)

// IntParser is integer field parser.
type IntParser struct {
	bp.BaseParser
	def   int
	upper int
	lower int
}

// Default set default return value when field is empty or nothing.
func (ip *IntParser) Default(d int) *IntParser {
	ip.def = d
	ip.HasDef = true
	return ip
}

// Upper set upper limitation.
func (ip *IntParser) Upper(u int) *IntParser {
	ip.upper = u
	ip.HasUpper = true
	return ip
}

// Lower set lower limitation.
func (ip *IntParser) Lower(l int) *IntParser {
	ip.lower = l
	ip.HasLower = true
	return ip
}

// Parse integer field.
func (ip *IntParser) Parse(v url.Values) (int, error) {
	q, err := strconv.Atoi(v.Get(ip.Field))
	if err != nil {
		if !ip.HasDef {
			return 0, errors.New("cannot find field value")
		}
		q = ip.def
	}
	if ip.HasUpper && ip.upper < q {
		q = ip.upper
	}
	if ip.HasLower && q < ip.lower {
		q = ip.lower
	}
	return q, nil
}
