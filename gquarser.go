package gquarser

import (
	"github.com/kudohamu/gquarser/bp"
	"github.com/kudohamu/gquarser/floatparser"
	"github.com/kudohamu/gquarser/floatsparser"
	"github.com/kudohamu/gquarser/intparser"
	"github.com/kudohamu/gquarser/intsparser"
	"github.com/kudohamu/gquarser/stringparser"
	"github.com/kudohamu/gquarser/stringsparser"
)

// NewStringParser return new string parser.
func NewStringParser(field string) *stringparser.StringParser {
	return &stringparser.StringParser{
		BaseParser: bp.BaseParser{
			Field: field,
		},
	}
}

// NewIntParser return new int parser.
func NewIntParser(field string) *intparser.IntParser {
	return &intparser.IntParser{
		BaseParser: bp.BaseParser{
			Field: field,
		},
	}
}

// NewFloatParser return new float parser.
func NewFloatParser(field string, bit int) *floatparser.FloatParser {
	return &floatparser.FloatParser{
		BaseParser: bp.BaseParser{
			Field: field,
		},
		Bit: bit,
	}
}

// NewStringsParser return new strings parser.
func NewStringsParser(field string) *stringsparser.StringsParser {
	return &stringsparser.StringsParser{
		BaseParser: bp.BaseParser{
			Field: field,
		},
		Delim: " ",
	}
}

// NewIntsParser return new strings parser.
func NewIntsParser(field string) *intsparser.IntsParser {
	return &intsparser.IntsParser{
		BaseParser: bp.BaseParser{
			Field: field,
		},
		Delim: " ",
	}
}

// NewFloatsParser return new floats parser.
func NewFloatsParser(field string, bit int) *floatsparser.FloatsParser {
	return &floatsparser.FloatsParser{
		BaseParser: bp.BaseParser{
			Field: field,
		},
		Bit:   bit,
		Delim: " ",
	}
}
