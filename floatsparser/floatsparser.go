package floatsparser

import (
	"errors"
	"net/url"
	"strconv"
	"strings"

	"github.com/kudohamu/gquarser/bp"
)

// FloatsParser is float field parser that have delimiter.
type FloatsParser struct {
	bp.BaseParser
	def   []float64
	Bit   int
	Delim string
	upper float64
	lower float64
}

// Default set default return value when field is empty or nothing.
func (fsp *FloatsParser) Default(d []float64) *FloatsParser {
	fsp.def = d
	fsp.HasDef = true
	return fsp
}

// Upper set upper limitation.
func (fsp *FloatsParser) Upper(u float64) *FloatsParser {
	fsp.upper = u
	fsp.HasUpper = true
	return fsp
}

// Lower set lower limitation.
func (fsp *FloatsParser) Lower(l float64) *FloatsParser {
	fsp.lower = l
	fsp.HasLower = true
	return fsp
}

// Delimiter set field delimiter.
func (fsp *FloatsParser) Delimiter(d string) *FloatsParser {
	fsp.Delim = d
	return fsp
}

// Parse strings field.
func (fsp *FloatsParser) Parse(v url.Values) ([]float64, error) {
	fs := []float64{}
	q := strings.Split(v.Get(fsp.Field), fsp.Delim)
	if len(q) == 1 && q[0] == "" {
		if !fsp.HasDef {
			return []float64{}, errors.New("cannot find field value")
		}
		fs = fsp.def
	} else {
		for k := range q {
			f, err := strconv.ParseFloat(q[k], fsp.Bit)
			if err != nil {
				return []float64{}, err
			}

			if fsp.HasUpper {
				if fsp.upper < f {
					f = fsp.upper
				}
			}
			if fsp.HasLower {
				if f < fsp.lower {
					f = fsp.lower
				}
			}
			fs = append(fs, f)
		}
	}

	return fs, nil
}
