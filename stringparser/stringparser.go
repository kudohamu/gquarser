package stringparser

import (
	"errors"
	"net/url"

	"github.com/kudohamu/gquarser/bp"
)

// StringParser is string field parser.
type StringParser struct {
	bp.BaseParser
	def string
}

// Default set default return value when field is empty or nothing.
func (sp *StringParser) Default(d string) *StringParser {
	sp.def = d
	sp.HasDef = true
	return sp
}

// Parse string field.
func (sp *StringParser) Parse(v url.Values) (string, error) {
	q := v.Get(sp.Field)
	if q == "" {
		if !sp.HasDef {
			return "", errors.New("cannot find field value")
		}
		q = sp.def
	}
	return q, nil
}
