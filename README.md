# gquarser
pyaaaaaaaaaaaaaaaaaaa

```golang
a, _ := url.Parse("?hoge=a")
huga1, err := NewStringParser("hoge").Parse(u.Query()) //huga1 == "hoge". err == nil
huga2, err := NewStringParser("huga").Parse(u.Query()) //err != nil
huga3, err := NewStringParser("huga").Default("huga!").Parse(u.Query()) //huga3 == "huga!". err is always nil

//other
v, _ := url.Parse("?foo=10")
a, _ := NewIntParser("foo").Default(0).Lower(-5).Upper(5).Parse(v.Query()) //a == 5

w, _ := url.Parse("?bar=-10.5")
b, _ := NewFloatParser("bar", 64).Default(0).Lower(-5.5).Parse(w.Query()) //b == -5.5

x, _ := url.Parse("?baz=p,i,y,o")
c, _ := NewStringsParser("baz").Default("aaa").Delimiter(",") c == ["p", "i", "y", "o"]
```
