package intsparser

import (
	"errors"
	"net/url"
	"strconv"
	"strings"

	"github.com/kudohamu/gquarser/bp"
)

// IntsParser is int field parser that have delimiter.
type IntsParser struct {
	bp.BaseParser
	def   []int
	Delim string
	upper int
	lower int
}

// Default set default return value when field is empty or nothing.
func (isp *IntsParser) Default(d []int) *IntsParser {
	isp.def = d
	isp.HasDef = true
	return isp
}

// Upper set upper limitation.
func (isp *IntsParser) Upper(u int) *IntsParser {
	isp.upper = u
	isp.HasUpper = true
	return isp
}

// Lower set lower limitation.
func (isp *IntsParser) Lower(l int) *IntsParser {
	isp.lower = l
	isp.HasLower = true
	return isp
}

// Delimiter set field delimiter.
func (isp *IntsParser) Delimiter(d string) *IntsParser {
	isp.Delim = d
	return isp
}

// Parse strings field.
func (isp *IntsParser) Parse(v url.Values) ([]int, error) {
	is := []int{}
	q := strings.Split(v.Get(isp.Field), isp.Delim)
	if len(q) == 1 && q[0] == "" {
		if !isp.HasDef {
			return []int{}, errors.New("cannot find field value")
		}
		is = isp.def
	} else {
		for k := range q {
			i, err := strconv.Atoi(q[k])
			if err != nil {
				return []int{}, err
			}

			if isp.HasUpper {
				if isp.upper < i {
					i = isp.upper
				}
			}
			if isp.HasLower {
				if i < isp.lower {
					i = isp.lower
				}
			}
			is = append(is, i)
		}
	}

	return is, nil
}
