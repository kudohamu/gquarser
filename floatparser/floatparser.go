package floatparser

import (
	"errors"
	"net/url"
	"strconv"

	"github.com/kudohamu/gquarser/bp"
)

// FloatParser is float field parser.
type FloatParser struct {
	bp.BaseParser
	Bit   int
	def   float64
	upper float64
	lower float64
}

// Default set default return value when field is empty or nothing.
func (fp *FloatParser) Default(d float64) *FloatParser {
	fp.def = d
	fp.HasDef = true
	return fp
}

// Upper set upper limitation.
func (fp *FloatParser) Upper(u float64) *FloatParser {
	fp.upper = u
	fp.HasUpper = true
	return fp
}

// Lower set lower limitation.
func (fp *FloatParser) Lower(l float64) *FloatParser {
	fp.lower = l
	fp.HasLower = true
	return fp
}

// Parse float field.
func (fp *FloatParser) Parse(v url.Values) (float64, error) {
	q, err := strconv.ParseFloat(v.Get(fp.Field), fp.Bit)
	if err != nil {
		if !fp.HasDef {
			return 0, errors.New("cannot find field value")
		}
		q = fp.def
	}
	if fp.HasUpper && fp.upper < q {
		q = fp.upper
	}
	if fp.HasLower && q < fp.lower {
		q = fp.lower
	}
	return q, nil
}
